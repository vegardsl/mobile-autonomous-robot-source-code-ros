# README #

### What is this repository for? ###

To track work done on the files within this repo.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The files withing this repo were developed for ROS Indigo (ros.org).
ROS indigo requires a compatable version of Ubuntu, for example 14.04.
The Bluetooth server in this project requires an installation of Qt5. 
 
1. Create a catkin workspace.
2. Clone the repo contents to the src folder within the workspace.
   the file structure should be
    * catkin_ws\
        + src\
            - mar
            - mar_gazebo
            - mar_2dnav
            - ...
3. In CMakeLists.txt in "bluetooth_server", locate:
  (CMAKE_PREFIX_PATH "/home/<user>/Qt/5.5/gcc_64/lib/cmake/Qt5"
  "/home/<user>/Qt/5.5/gcc_64/lib/cmake/Qt5Core"
  "/home/<user>/Qt/5.5/gcc_64/lib/cmake/Qt5Bluetooth"
 
  Change these paths to the correct paths on your system.
4. Run catkin_make to build the project.
5. Build and install rtabmap_ros from source by following
   the guidelines on https://github.com/introlab/rtabmap_ros


### Contribution guidelines ###

* TODO

### Who do I talk to? ###

* Tor Onshus or Vegard S. Lindrup